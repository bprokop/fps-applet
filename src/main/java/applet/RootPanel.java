package applet;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import name.prokop.bart.fps.FiscalPrinter;
import name.prokop.bart.fps.datamodel.SlipExamples;
import name.prokop.bart.fps.util.PortEnumerator;

public class RootPanel extends JPanel {

    private final JComboBox<String> cbSerialPort;
    private final JComboBox<FiscalPrinter.Type> cbDrivers;
    private final JTextArea textArea;
    private final JButton jButtonPrint;

    public RootPanel() {
        super(new BorderLayout());
        setOpaque(false);

        final JPanel topPanel = new JPanel(new GridLayout(2, 2));
        topPanel.setBackground(Color.WHITE);
        topPanel.add(new JLabel("Port RS-232"));
        cbSerialPort = new JComboBox<>();
        cbSerialPort.setBackground(Color.WHITE);
        cbSerialPort.setEditable(true);
        topPanel.add(cbSerialPort);
        topPanel.add(new JLabel("Typ drukarki"));
        cbDrivers = new JComboBox<>();
        cbDrivers.setBackground(Color.WHITE);
        for (FiscalPrinter.Type type : FiscalPrinter.Type.values()) {
            cbDrivers.addItem(type);
        }
        if (Applet.FISCAL_PRINTER_TYPE != null) {
            cbDrivers.setSelectedItem(Applet.FISCAL_PRINTER_TYPE);
        }
        topPanel.add(cbDrivers);
        add(topPanel, BorderLayout.NORTH);

        textArea = new JTextArea();
        add(textArea, BorderLayout.CENTER);

        jButtonPrint = new JButton("Drukuj paragon");
        jButtonPrint.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                print();
                jButtonPrint.setEnabled(false);
            }
        });
        add(jButtonPrint, BorderLayout.SOUTH);

        new Thread(new DiscoverSerialPorts()).start();
        new Thread(new RetrieveSlip()).start();
    }

    private void print() {
        if (Applet.SLIP == null) {
            JOptionPane.showConfirmDialog(this, "Nie odczytano (jeszcze) paragonu");
            return;
        }
        final FiscalPrinter.Type driver = (FiscalPrinter.Type) cbDrivers.getSelectedItem();
        final String port = (String) cbSerialPort.getEditor().getItem();
        textArea.append("Port/Sterownik: " + port + "/" + driver + "\n");

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    info("Wybrano: " + driver + " @ " + port);
                    FiscalPrinter fiscalPrinter = driver.getFiscalPrinter(port);
                    fiscalPrinter.print(Applet.SLIP);
                    info("Wydrukowano paragon");
                } catch (Throwable t) {
                    info("Bład podczas drukowania");
                    info(t);
                }
            }
        }).start();
    }

    private void info(final String s) {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {

                @Override
                public void run() {
                    textArea.append(s + "\n");
                }
            });
        } catch (InterruptedException | InvocationTargetException e) {
        }
    }

    private void info(final Throwable t) {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {

                @Override
                public void run() {
                    StringWriter s = new StringWriter();
                    t.printStackTrace(new PrintWriter(s));
                    textArea.append(s.toString() + "\n");
                }
            });
        } catch (InterruptedException | InvocationTargetException e) {
        }
    }

    private class DiscoverSerialPorts implements Runnable {

        @Override
        public void run() {
            try {
                final String[] serialPortList = PortEnumerator.getSerialPortList();
                info("Wykryto porty szeregowe: " + Arrays.toString(serialPortList));
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {

                        @Override
                        public void run() {
                            for (String s : serialPortList) {
                                cbSerialPort.addItem(s);
                            }
                        }
                    });
                } catch (InterruptedException | InvocationTargetException e) {
                    info(e);
                }
            } catch (Throwable t) {
                info(t);
            }

            if (Applet.COM_PORT != null) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {

                        @Override
                        public void run() {
                            cbSerialPort.getEditor().setItem(Applet.COM_PORT);
                        }
                    });
                } catch (InterruptedException | InvocationTargetException e) {
                    info(e);
                }
            }
        }

    }

    private class RetrieveSlip implements Runnable {

        @Override
        public void run() {
            try {
                Applet.SLIP = SlipExamples.getOneCentSlip();
                if (Applet.STANDALONE_SLIP_URL != null && Applet.STANDALONE_SLIP_URL.trim().length() > 0) {
                    info("Odczytywania paragonu z URLa: " + Applet.STANDALONE_SLIP_URL);
                    Applet.SLIP = Standalone.readSlip();
                }
                if (Applet.API_KEY != null && Applet.API_KEY.trim().length() > 0 && Applet.DOCUMENT_ID != null && Applet.DOCUMENT_ID.trim().length() > 0) {
                    info("Próba odczytu paragonu z chmury...");
                    Applet.SLIP = Cloud.readSlip();
                }
                info("Odczytano paragon: " + Applet.SLIP);
            } catch (Throwable t) {
                info("Błąd podczas pobierania paragonu");
                info(t);
            }
        }

    }
}
