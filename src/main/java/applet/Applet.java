package applet;

import java.lang.reflect.InvocationTargetException;
import javax.swing.JApplet;
import javax.swing.SwingUtilities;
import name.prokop.bart.fps.FiscalPrinter;
import name.prokop.bart.fps.datamodel.Slip;

public class Applet extends JApplet {

    public static String API_KEY;
    public static String DOCUMENT_ID;
    public static String STANDALONE_SLIP_URL;
    public static FiscalPrinter.Type FISCAL_PRINTER_TYPE;
    public static String COM_PORT;
    public static Slip SLIP;

    @Override
    public void init() {
        API_KEY = getParameter("api-key");
        DOCUMENT_ID = getParameter("documentId");
        STANDALONE_SLIP_URL = getParameter("SlipURL");
        if (getParameter("driverType") != null) {
            FISCAL_PRINTER_TYPE = FiscalPrinter.Type.valueOf(getParameter("driverType"));
        }
        COM_PORT = getParameter("comPort");
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    setContentPane(new RootPanel());
                }
            });
        } catch (InterruptedException | InvocationTargetException e) {
            System.err.println("createGUI didn't complete successfully");
        }
    }

}
