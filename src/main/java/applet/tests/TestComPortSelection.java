package applet.tests;

import java.util.HashMap;
import java.util.Map;

public class TestComPortSelection {

    public static void main(String... args) {
        Map<String, String> params = new HashMap<>();
        params.put("comPort", "COM51");
        BrowserEmulator.invoke(params);
    }
}
