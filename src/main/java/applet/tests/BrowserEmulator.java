package applet.tests;

import applet.Applet;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;
import javax.swing.JFrame;

public class BrowserEmulator {

    public static void invoke(final Map<String, String> map) {
        final JFrame jf = new JFrame();
        final Applet applet = new Applet() {

            @Override
            public String getParameter(String name) {
                return map.get(name);
            }

        };
        jf.getContentPane().add(applet);
        jf.setBounds(10, 10, 400, 400);
        jf.setVisible(true);
        applet.init();
        applet.start();
        jf.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                applet.stop();
                applet.destroy();
                jf.dispose();
            }

        });
    }
}
