package applet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import name.prokop.bart.fps.datamodel.Slip;
import org.json.JSONException;
import org.json.JSONObject;

public class Standalone {

    public static Slip readSlip() {
        try {
            URL url = new URL(Applet.STANDALONE_SLIP_URL);
            URLConnection conn = url.openConnection();

            StringBuilder sb = new StringBuilder();
            try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"))) {
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine).append('\n');
                }
            }

            JSONObject jsono = new JSONObject(sb.toString());
            return new Slip(jsono);
        } catch (IOException | JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
