package applet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import name.prokop.bart.fps.datamodel.Slip;
import name.prokop.bart.fps.datamodel.VATRate;
import org.json.JSONException;
import org.json.JSONObject;

public class Cloud {

    private static final String ISOCOM = "c6406622-4dab-47d8-9034-cdc7e69ada51";

    public static Slip readSlip() {
        try {
            String urlString = "https://natanedwin.appspot.com/api/fps";
            urlString = urlString.concat("/getDocument");
            urlString = urlString.concat("?documentId=");
            urlString = urlString.concat(Applet.DOCUMENT_ID);
            URL url = new URL(urlString);

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestProperty("API-Key", Applet.API_KEY);
            conn.setRequestMethod("GET");

            StringBuilder sb = new StringBuilder();
            try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"))) {
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine).append('\n');
                }
            }

            JSONObject jsono = new JSONObject(sb.toString());
            Slip slip = new Slip(jsono);
            if (ISOCOM.equalsIgnoreCase(Applet.API_KEY)) {
                slip.addLine("Wersja demo", 1, 0.01, VATRate.VAT23);
                slip.addLine("Kup licencje", 1, 0.01, VATRate.VAT23);
                slip.getSlipPayments().clear();
            }
            return slip;
        } catch (IOException | JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
